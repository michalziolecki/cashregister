#include <iostream>
#include "C:\TeldatT\Menu.cpp"
#include "C:\TeldatT\IOFilesMethod.cpp"
//#include "C:\TeldatT\ProductDetailsToReceipt.cpp"
#pragma once

using namespace std;

class CashierManager{
private:
    Menu* cashierMenu;
    vector<ProductDetailsConteiner> productlistFromDB;
    int idReceiptNumber = 0;
    IOFilesMethod ioFilesMethod;
    HANDLE cashierConsole;

public:

    CashierManager(Menu* menu, HANDLE &mainConsole){
        cashierMenu = menu;
        cashierConsole = mainConsole;
        productlistFromDB = ioFilesMethod.readFromDataBaseFile();
    }

    void selectDecision(){
        bool flag = true;
        while( flag ){
            system("CLS");
            cashierMenu ->workingMenu();
            // here doesnt work char type. problem with input (mess on input, program dont read firt char ex. need to write '11')
            string input;// = "";
            getline(cin, input);
            switch(input[0]){
                case '1':
                    addProductToBasket();
                    break;
                case '2':
                    flag = false;
                    break;
                default:
                    // this 'if' save this code to run when 'getline' doesn't started
                    if(!input.empty()){
                        cout << "Wybierz jedna z 2 wlasciwych opcji 1 | 2 :" << endl;
                        cout << "Najpierw nadus 'enter', aby wrocic do menu \n";
                        // this loop waiting to press enter key
                        while (true) {
                            string temp = "";
                            getline(cin, temp);
                            if(temp.empty()) break;
                        }
                    }
                    break;
            }
        }
    }

    void addProductToBasket(){
        vector<ProductDetailsToReceipt> basketWithProduct;
        idReceiptNumber++;
        while(true){
            system("CLS");
            SetConsoleTextAttribute(cashierConsole, YELLOW);
            cout << "Zawartosc koszyka: \n" << endl;
            if(basketWithProduct.size() == 0) cout << " ----------\n koszyk pusty\n  -----------\n ";
            double sum = 0.0;
            for(int i = 0; i < basketWithProduct.size(); i++ ) {
                cout<< "KOD  | "<<" NAZWA |  " << "CENA/100g | " << " WAGA (g) | "<< " KOSZT (zl) |\n"<<endl;
                cout << " " <<
                     basketWithProduct.at(i).codeOfProduct << "    " <<
                     basketWithProduct.at(i).name << "      " <<
                     basketWithProduct.at(i).pricePer100g << "         " <<
                     basketWithProduct.at(i).weigh << "         " <<
                     (basketWithProduct.at(i).weigh/100) * basketWithProduct.at(i).pricePer100g << endl << endl;
                sum += (basketWithProduct.at(i).weigh/100) * basketWithProduct.at(i).pricePer100g;
            }
            cout<< "--------------------------------SUMA: " << sum <<endl<<endl<<endl;

            SetConsoleTextAttribute(cashierConsole, GREEN);
            cashierMenu ->setCodeMenu();
            string addOrPrint = "";
            cin >> addOrPrint;
            double weight = 0.0;
            if(addOrPrint == "p"){
                cout << "Trwa drukowanie paragonu... \n";
                ioFilesMethod.printToReceiptFile(basketWithProduct,idReceiptNumber);
                break;
            }
            else{
                cout << "Dodano produkt o id: " << addOrPrint << endl;
                cout<< "Podaj wage produktu: " << endl;
                cin >> weight;
                basketWithProduct.push_back(addToBasket(addOrPrint, weight));
            }

        }
        // clear basket
        /*while(basketWithProduct.size() != 0)
            basketWithProduct.erase(basketWithProduct.erase(basketWithProduct.begin()));*/
    }


    ProductDetailsToReceipt addToBasket(string code, double weight){
        ProductDetailsToReceipt productDetails;
        for(int i = 0; i < productlistFromDB.size(); i++){
            if(code == productlistFromDB.at(i).codeOfProduct){
                productDetails.codeOfProduct = productlistFromDB.at(i).codeOfProduct;
                productDetails.name = productlistFromDB.at(i).name;
                productDetails.pricePer100g = productlistFromDB.at(i).pricePer100g;
                productDetails.weigh = weight;
                break;
            }
        }

        return  productDetails;
    }

};
