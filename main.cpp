#include <iostream>
#include <cstdio>
#include <windows.h>
#include "Menu.cpp"
#include "C:\TeldatT\IOFilesMethod.cpp"
#include <vector>
#include "DBmanager.cpp"
#include "CashierManager.cpp"
//#include "ColorEnum.cpp"
//#pragma once
using namespace std;

/*
 * Function to set color of text in output (console)
 * */
HANDLE setConsoleColor(ColorEnum color){
    HANDLE mainConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(mainConsole,color);
    return mainConsole;
}

int main() {

    /**
     *  Created menu with color interface in constructor (eg. it will be rebuild in future)
     * */
    HANDLE mainConsoleHandle = setConsoleColor(WHITE);
    Menu menu(mainConsoleHandle);
    //menu.mainMenu();
    Menu* indMenu = &menu;
    bool flag = true;
    DBmanager dbManager(indMenu, mainConsoleHandle);
    CashierManager cashierManager(indMenu, mainConsoleHandle);

    // main loop in program with main menu to show on desktop
    while(flag){
        system("CLS");
        indMenu ->mainMenu();
        int inPut;
        cin >> inPut;

        switch (inPut){
            case 1:
                cashierManager.selectDecision();
                break;
            case 2:
                dbManager.selectDecision();
                break;
            case 3:
                system("CLS");
                cout << "Nastapi wylaczenie programu, do zobaczenia! \n";
                _sleep(3000); // i know that is deprecated method but, here is finish work program and we have time :)
                flag = false;
                break;
            default:
                cout << "Podaj poprawna cyfre 1 | 2 | 3 \n";
                cout << "Najpierw nadusc dowoony przycisk, aby wrocic do menu \n";
                getchar();
                break;
        }
    }

    return 0;
}

