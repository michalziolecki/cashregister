#include <iostream>

using namespace std;
/*
 * Container to details of products, using like a type of list and later print this list to DB file;
 * */
struct ProductDetailsConteiner{
    string codeOfProduct;
    string name;
    double pricePer100g;
};