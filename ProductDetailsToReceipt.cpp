#include <iostream>

using namespace std;
/*
 * Container to details of products, using like a type of list and later print this list to receipt;
 * */
struct ProductDetailsToReceipt{
    string codeOfProduct;
    string name;
    double pricePer100g;
    double weigh;
};
