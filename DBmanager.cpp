#include <iostream>
#include "Menu.cpp"
#include <cstdio>
#include <vector>
#include <limits>
//#include "C:\TeldatT\ProductDetailsConteiner.cpp"
#include "C:\TeldatT\IOFilesMethod.cpp"
#pragma once

using namespace std;

class DBmanager{
private:
    Menu* menuDB;
    vector<ProductDetailsConteiner> productlist;
    IOFilesMethod ioFilesMethod;
    HANDLE menagerConsole;

public:
    DBmanager(Menu* menu, HANDLE &mainConsole){
        menuDB = menu ;
        menagerConsole = mainConsole;
    }

    void selectDecision(){
        bool flag = true;
        while( flag ){
            system("CLS");
            menuDB ->dataBaseMenu();
            //SetConsoleTextAttribute(menagerConsole, RED);
            // here doesnt work char type. problem with input (mess on input, program dont read firt char ex. need to write '11')
            string input;// = "";
            getline(cin, input);
            switch(input[0]){
                case '1':
                    selectAllProductFromFile();
                    //getchar();
                    break;
                case '2':
                    addProductsToFile();
                    break;
                case '3':
                    dropProdutcsFromFile();
                    break;
                case '4':
                    flag = false;
                    break;
                default:
                    // this 'if' save this code to run when 'getline' doesn't started
                    if(!input.empty()){
                        cout << "Wybierz jedna z 4 wlasciwych opcji 1 | 2 | 3 | 4:" << endl;
                        cout << "Najpierw nadusc 'enter', aby wrocic do menu \n";
                        // this loop waiting to press enter key
                        while (true) {
                            string temp = "";
                            getline(cin, temp);
                            if(temp.empty()) break;
                        }
                    }
                    break;
            }
        }
    }

    void selectAllProductFromFile(){
        system("CLS");
        SetConsoleTextAttribute(menagerConsole, YELLOW);
        cout << "Produkty w bazie to: \n";
        cout << "KOD: " << "NAZWA " << "CENA/100g \n";
        productlist = ioFilesMethod.readFromDataBaseFile();
        for(int i = 0; i < productlist.size(); i++){
            cout << productlist.at(i).codeOfProduct
                    << "  " << productlist.at(i).name
                 << "  " << productlist.at(i).pricePer100g;
            cout << endl;
        }
        SetConsoleTextAttribute(menagerConsole, GREEN);
        cout << "\n Nadus enter przycisk aby wrocic do menu \n";
        // this loop waiting to press enter key
        while (true) {
            string temp = "";
            getline(cin, temp);
            if(temp.empty()) break;
        }
    }

    void addProductsToFile(){
        system("CLS");
        ProductDetailsConteiner productDetailsConteiner;
        vector<ProductDetailsConteiner> productlistToAdd;
        cout << "Ile produktow chcesz dodac do bazy?" << endl;
        string code = "";
        string name = "";
        double cost = 0.0;
        int howMany = 0;
        cin >> howMany;
        for(int i = 0; i < howMany; i++){
            cout << "Podaj kod produktu: "<< endl;
            cin >> code;
            productDetailsConteiner.codeOfProduct = code;

            cout << "Podaj nazwe: "<< endl;
            cin >> name;
            productDetailsConteiner.name = name;

            cout << "Podaj koszt / 100g: "<< endl;
            cin >> cost;
            productDetailsConteiner.pricePer100g = cost;

            productlistToAdd.push_back(productDetailsConteiner);
        }

        cout << "Dodaje produkty do bazy ... \n" ;
        ioFilesMethod.printToDataBaseFile(productlistToAdd);
    }

    void dropProdutcsFromFile(){
        vector<string> codeToDrop;
        cout << "Ile produktow chcesz usunac?" << endl;
        int howMany;
        cin >> howMany;
        string codeOfProduct = "";

        for(int i = 1; i <= howMany; i++){
            cout << "Podaj kod "<< i << "produktu do usuniecia" << endl;
            cin >> codeOfProduct;
            codeToDrop.push_back(codeOfProduct);
        }

        ioFilesMethod.dropFromDataBaseFile(codeToDrop);

    }


};


