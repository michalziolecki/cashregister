#include <iostream>

using namespace std;

enum ColorEnum{
    BLACK = 0,
    RED = 4,
    DARK_BLUE = 1,
    GREEN = 10,
    WHITE = 7,
    GRAY = 8,
    YELLOW = 14
};
