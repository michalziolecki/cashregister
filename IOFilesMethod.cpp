#include <iostream>
#include <fstream>
#include <C:\TeldatT\ProductDetailsConteiner.cpp>
#include <vector>
#include <string>
#include <sstream>
#include <C:\TeldatT\ProductDetailsToReceipt.cpp>
#pragma once

using namespace std;

class IOFilesMethod{
private:
    const string NAME_OF_DATABASE_FILE = "DataBase.txt";
    const string NAME_OF_RECEIPT_FILE = "Receipt";

    vector<ProductDetailsConteiner> listOfProduct;


public:
    IOFilesMethod(){}
    /*
    * Method to print information about product into file (data base)
    * */
    void printToDataBaseFile(vector<ProductDetailsConteiner> productList){
        listOfProduct = productList;
        vector<ProductDetailsConteiner> previousProductList;
        previousProductList = readFromDataBaseFile();
        //-- create or open file - receipt
        fstream dataBaseFile;
        dataBaseFile.open(NAME_OF_DATABASE_FILE, ios::out);
        if(dataBaseFile.good()){

            for(int j = 0; j < previousProductList.size(); j++){

                dataBaseFile << previousProductList.at(j).codeOfProduct << " "
                             << previousProductList.at(j).name << " "
                             << previousProductList.at(j).pricePer100g<< "\n";
                //-- erase useless fields
                //previousProductList.erase(previousProductList.begin());
            }

            while(listOfProduct.size() != 0){

                bool exist = false;
                for(int i = 0; i < previousProductList.size(); i++){
                    if(listOfProduct.at(0).codeOfProduct == previousProductList.at(i).codeOfProduct){
                        cout << "Produkt o tym kodzie '"<< listOfProduct.at(0).codeOfProduct <<"' juz istnieje" <<endl;
                        exist = true;
                       // break;
                    }
                }
                if(exist == false){
                    dataBaseFile << listOfProduct.at(0).codeOfProduct << " "
                                 << listOfProduct.at(0).name << " "
                                 << listOfProduct.at(0).pricePer100g<< "\n";
                }
                //-- erase useless fields
                listOfProduct.erase(listOfProduct.begin());
                delete &exist;
            }
            dataBaseFile.close();
            //-- clear memory
            previousProductList.clear();
            listOfProduct.clear();
            productList.clear();
        }
        else{
            cout << "Nie udalo sie zapisac produktow do Bazu Danych w pliku" << endl;
        }

    }

    /*
     * Method to create receipt
     * */
    void printToReceiptFile(vector<ProductDetailsToReceipt> productList, int receiptID){
        vector<ProductDetailsToReceipt> listOfProductToReceipt;
        listOfProductToReceipt = productList;
        fstream ReceiptFile;
        //-- create name of file
        double sumOfPrices = 0.0;
        ostringstream toStringId;
        toStringId << receiptID;
        string id = toStringId.str();
        toStringId.erase_event;
        string nameOfFile = NAME_OF_RECEIPT_FILE + id + ".txt";
        //-- create or open file - receipt
        ReceiptFile.open(nameOfFile, ios::out);
        if(ReceiptFile.good()){
            int i = 1;
            while(listOfProductToReceipt.size() != 0){
                //-- create fields
                double thePriceForThePurchase = listOfProductToReceipt.at(0).pricePer100g * (listOfProductToReceipt.at(0).weigh/100);
                sumOfPrices += thePriceForThePurchase;
                //--print to file
                ReceiptFile << i << "." << " "
                            << listOfProductToReceipt.at(0).name << " "
                            << listOfProductToReceipt.at(0).pricePer100g << " "
                            << listOfProductToReceipt.at(0).weigh << " "
                            << thePriceForThePurchase <<"\n";
                //-- erase useless fields
                listOfProductToReceipt.erase(listOfProductToReceipt.begin());
                i++;
            }
            ReceiptFile << "-----------SUMA:" << " " << sumOfPrices;
            ReceiptFile.close();
            //-- clear memory
            delete &i;
            productList.clear();
            listOfProductToReceipt.clear();
            nameOfFile.erase();
            id.erase();
        }
        else{
            cout << "Nie udalo sie zapisac produktow do paragonu" << endl;
        }
    }

    /*
     * Method to read fields of object ProductDetailsConteiner
     * */
    vector<ProductDetailsConteiner> readFromDataBaseFile(){
        //here i using stream to read information from file

        string tempCodeOfProduct = "0";
        vector<ProductDetailsConteiner> listOfProductFromDB;
        ProductDetailsConteiner productDetailsConteiner;
        fstream fileOpener;
        fileOpener.open(NAME_OF_DATABASE_FILE);
        if(fileOpener.good()){

            while(!fileOpener.eof()){

                fileOpener >> productDetailsConteiner.codeOfProduct
                           >> productDetailsConteiner.name
                           >> productDetailsConteiner.pricePer100g;
                // condition protecting against object repetition in stream
                if(tempCodeOfProduct != productDetailsConteiner.codeOfProduct){
                    listOfProductFromDB.push_back(productDetailsConteiner);
                }
                tempCodeOfProduct = productDetailsConteiner.codeOfProduct;
            }
            fileOpener.close();

        }
        else{
            cout << "Nie udalo sie odczytac danych z pliku." <<endl;
        }
        if(listOfProductFromDB.size() == 0) {
            cout << "Baza danych w pliku jest pusta." <<endl;
        }
        //cout<<  "end of reading - size of list:" << listOfProductFromDB.size() <<endl;
        return listOfProductFromDB;
    }

    /*
    * Method to drop information about product in file (data base)
    * */
    void dropFromDataBaseFile(vector<string> productList){

        vector<ProductDetailsConteiner> previousProductList;
        previousProductList = readFromDataBaseFile();
        //-- create or open file - receipt
        fstream dataBaseFile;
        dataBaseFile.open(NAME_OF_DATABASE_FILE, ios::out);
        if(dataBaseFile.good()){

            bool flag = false;
            for(int i = 0; i < productList.size(); i++){
                for(int j = 0; j < previousProductList.size(); j++){
                    if(productList.at(i) == previousProductList.at(j).codeOfProduct){
                        cout << "Produkt o kodzie: "
                             << previousProductList.at(j).codeOfProduct << "zostanie usuniety \n";
                        previousProductList.erase(previousProductList.begin() + j);
                        flag = true;
                        break;
                    }
                }
            }

            if(flag == false) cout << "Nie usunieto nic z bazy danych, sprawdz poprawnosc kodow" <<endl;

            while(previousProductList.size() != 0){
                dataBaseFile << previousProductList.at(0).codeOfProduct << " "
                             << previousProductList.at(0).name << " "
                             << previousProductList.at(0).pricePer100g<< "\n";
                //-- erase useless fields
                previousProductList.erase(previousProductList.begin());
            }
            dataBaseFile.close();
            //-- clear memory
            previousProductList.clear();
            productList.clear();
        }
        else{
            cout << "Nie udalo sie usunac produktow z Bazu Danych w pliku" << endl;
        }

    }

};
