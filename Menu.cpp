#include <iostream>
#include <windows.h>
#include "ColorEnum.cpp"
#pragma once

using namespace std;

class Menu{
private:
    HANDLE menuConsole;
    ColorEnum colorEnumText;
    ColorEnum colorEnumBackGround;

public:
    Menu(){
        colorEnumText = WHITE;
        colorEnumBackGround = BLACK;
    }

    Menu(HANDLE &ConsoleMain){
        menuConsole = ConsoleMain;
        colorEnumText = GREEN;
        colorEnumBackGround = BLACK;
    }

    void setTextColor(ColorEnum color){
        SetConsoleTextAttribute(menuConsole, colorEnumText);
    }

    void mainMenu(){
        //Set color input text
        setTextColor(colorEnumText);

        cout << " Witaj, w glownym menu! \n \n";
        cout << "Wybierz jedna z ponizszych operacji: \n " ;
        cout << "1) Rozpocznij prace kasjera. \n ";
        cout << "2) Rozpocznij prace z baza produktow. \n ";
        cout << "3) Zakoncz prace. \n ";
    }

    void dataBaseMenu(){
        cout << "Menu Bazy Produktow: \n";
        cout << "1) Wyswietl produkty znajdujace sie w bazie \n";
        cout << "2) Dodaj produkty do bazy danych \n";
        cout << "3) Usun produkty z bazy danych \n";
        cout << "4) Wroc do menu glownego \n";
    }

    void workingMenu(){
        cout << "Menu kasjera: \n";
        cout << "1) Rozpocznij naliczanie produktow \n";
        cout << "2) Wroc do menu glownego \n";
    }

    void setCodeMenu(){
        cout << "Naliczasz klienta: \n";
        cout << "Podaj kod kolejnego produktu, ";
        cout << "lub wpisz 'p' aby wydrukowac paragon. \n";
    }


    const void *getMenuConsole() const {
        return menuConsole;
    }

    ColorEnum getColorEnumText() const {
        return colorEnumText;
    }

    void setColorEnumText(ColorEnum colorEnumText) {
        Menu::colorEnumText = colorEnumText;
    }

    ColorEnum getColorEnumBackGround() const {
        return colorEnumBackGround;
    }

    void setColorEnumBackGround(ColorEnum colorEnumBackGround) {
        Menu::colorEnumBackGround = colorEnumBackGround;
    }
};

